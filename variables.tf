###   _____              __ _
###  /  __ \            / _(_)
###  | /  \/ ___  _ __ | |_ _  __ _
###  | |    / _ \| '_ \|  _| |/ _` |
###  | \__/\ (_) | | | | | | | (_| |
###   \____/\___/|_| |_|_| |_|\__, |
###                            __/ |
###                           |___/

variable "argocd_namespace" {
  description = "The ArgoCD instance to deploy to"
  default = "argocd"
}

variable "name" {
  description = "The application name"
  default = "minio-gateway"
}

variable "namespace" {
  description = "The Kubernetes namespace to deploy to"
}

variable "helm_chart_repo" {
  type = string
  default = "https://statcan.github.io/charts"
  description = "The Chart location for MinIO-Gateway"
}

variable "helm_chart_name" {
  type = string
  default = "minio-gateway"
  description = "The Chart name for MinIO-Gateway"
}

variable "helm_chart_version" {
  type = string
  description = "The Chart version for MinIO-Gateway"
}

variable "minio_replicas" {
  type = number
  description = "Number of MinIO pods to deploy"
}

variable "etcd_replicas" {
  type = number
  description = "Number of etcd pods to deploy"
}

variable "azure_storage_account_name" {
  type = string
  description = "The Azure Storage Account name"
}

variable "azure_storage_account_key" {
  type = string
  description = "The Azure Storage Account key"
}

variable "minio_image_repo" {
  type = string
  default = "docker.io"
  description = "a minio image, i.e. minio/minio"
}

variable "minio_image_name" {
  type = string
  default = "bitnami/minio"
  description = "a minio image, i.e. minio/minio"
}

variable "minio_image_tag" {
  type = string
  default = "2021.6.17-debian-10-r0"
  description = "A minio image tag, i.e. latest"
}
