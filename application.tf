# NOTE: If you change the name of the manifest, I am
# not sure if the old one will be deleted.
#
# So change ${var.name} with care.

resource "kubectl_manifest" "gateway_application" {
  yaml_body = <<YAML
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: ${var.name}
  namespace: ${var.argocd_namespace}
spec:
  project: default
  destination:
    namespace: ${var.namespace}
    server: https://kubernetes.default.svc
  source:
    repoURL: ${var.helm_chart_repo}
    chart: ${var.helm_chart_name}
    targetRevision: ${var.helm_chart_version}
    helm:
      values: |
        ## Etcd configuration
        etcd:
          replicaCount: ${var.etcd_replicas}
        ## MinIO(R) Gateway configuration
        minio:
          gateway:
            enabled: true
            type: azure
            replicaCount: ${var.minio_replicas}
            auth:
              azure:
                storageAccountName: "${var.azure_storage_account_name}"
                storageAccountKey: "${var.azure_storage_account_key}"
          extraEnv:
            - name: MINIO_ETCD_ENDPOINTS
              value: http://${var.name}-etcd-headless:2379
          image:
            registry: ${var.minio_image_repo}
            repository: ${var.minio_image_name}
            tag: ${var.minio_image_tag}
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
    - CreateNamespace=true
YAML

  sensitive_fields = ["spec.source.helm.values"]
}
