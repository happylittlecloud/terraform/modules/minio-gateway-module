resource "time_sleep" "wait_30_seconds" {
  depends_on = [kubectl_manifest.gateway_application]
  create_duration = "45s"
}

# We wait 45s after the creation of the gateway
# for everything to boot and for a new secret to
# be created. We wait for the sleep to finish
# before sourcing the secert.
data "kubernetes_secret" "minio_secret" {
  metadata {
    name      = var.name
    namespace = var.namespace
  }
  depends_on = [
    time_sleep.wait_30_seconds
  ]
}

